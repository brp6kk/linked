package eyc.input;

import java.util.*;
import eyc.exception.*;

/**
 * Class that contains several static methods that can be used to obtain
 * numeric user input.
 */
public class NumIn
{
    /**
     * Prompts user to enter any integer, not leaving the method until
     * an integer is obtained.
     * 
     * @param prompt The prompt used to describe to user what to enter.
     * 
     * @param input The Scanner used to read in user input.
     * 
     * @return The necessary integer obtained from the user.
     */
    public static int getInt(String prompt, Scanner input)
    {
        // Variable declarations.
        boolean keepLooping = true; // Loop control variable.
        String userInput;
        int num = 0;
        
        // Loops until desired user input is obtained.
        do
        {
            // Prompts user for and obtains input.
            System.out.print(prompt);
            userInput = input.nextLine();
            
            // Attempts to convert user input to an int.
            // If this conversion is unsuccessful, the loop executes
            // at least one more time after a warning message is 
            // displayed.
            // If the conversion is successful, the loop is exitted.
            try
            {
                num = Integer.parseInt(userInput);
                keepLooping = false;
            }
            catch (NumberFormatException e)
            {
                System.out.println("Input must be an integer.");
                keepLooping = true;
            }
        } while (keepLooping);
        
        return (num);
    }
    
    /**
     * Calls getInt(String, Scanner) with a default prompt.
     * 
     * @param input The Scanner used to read in user input.
     * 
     * @return The necessary integer obtained from the user.
     */
    public static int getInt(Scanner input)
    {
        // Variable declarations.
        int num = 0;
        String prompt = "Enter an integer: "; // Default prompt for getInt.
        
        // Calls other getInt method using default prompt to obtain number.
        num = getInt(prompt, input);
        
        return (num);
    }
    
    /**
     * Prompts user to enter an integer between two values, 
     * not leaving the method until an integer in the proper
     * range is obtained.
     * 
     * @param lower The lowest valid value.
     * 
     * @param upper The highest valid value.
     * 
     * @param prompt The prompt used to describe to user what to enter.
     * 
     * @param input The Scanner used to read in user input.
     * 
     * @return The necessary integer obtained from the user.
     */
    public static int getInt(int lower, int upper, String prompt, Scanner input)
    {
        // Variable declaration.
        boolean keepLooping = true; // Loop control variable.
        int num = 0;
        
        // Loops until desired user input is obtained.
        do
        {
            try
            {
                // Obtains a number from the user.
                num = getInt(prompt, input);
                
                // Number cannot be outside of the desired range.
                if (num < lower || num > upper)
                {
                    throw new ImproperRangeException(lower, upper);
                }
                
                // Getting past the previous if statement means that a
                // proper number was obtained, so the loop does not need
                // to continue.
                keepLooping = false;
            }
            catch (ImproperRangeException ex)
            {
                System.out.println(ex);
                keepLooping = true;
            }
        } while (keepLooping);
        
        return (num);
    }
    
    /**
     * Calls getInt(int, int, String, Scanner) with a default prompt.
     * 
     * @param lower The lowest valid value.
     * 
     * @param upper The highest valid value.
     * 
     * @param input The Scanner used to read in user input.
     * 
     * @return The necessary integer obtained from the user.
     */
    public static int getInt(int lower, int upper, Scanner input)
    {
        // Variable declarations.
        int num = 0;
        String prompt = ("Enter an integer between " + lower + " and " + 
                         upper + ": "); // Default prompt for getInt.
        
        // Calls other getInt method using default prompt to obtain number.
        num = getInt(lower, upper, prompt, input);
        
        return (num);
    }
}
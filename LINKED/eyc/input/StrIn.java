package eyc.input;

import java.util.*;
import eyc.exception.*;

/**
 * Class that contains several static methods that can be used to obtain
 * String user input.
 */
public class StrIn
{
    /**
     * Prompts user to enter answer a yes or no question, not leaving the
     * method until either "y" (for yes) or "n" (for no) is entered.
     * 
     * @param prompt The prompt that the user is to respond to.
     * 
     * @param input The Scanner used to read in user input.
     * 
     * @return True if the user entered "y"; false if the user
     * entered "n".
     */
    public static boolean getAnswer(String prompt, Scanner input)
    {
        // Variable declarations.
        boolean keepLooping = true; // Loop control variable.
        String userInput;
        boolean userChoice = false;
        
        // Loops until desired user input is obtained.
        do
        {
            try
            {
                // Prompts user for and obtains input.
                // Input is converted to all lowercase so that
                // it isn't case sensative.
                System.out.print(prompt + " (Y/N): ");
                userInput = input.nextLine().toLowerCase();
            
                // Y = true; N = false
                // Y and N are the only valid input values; any other input
                // is not acceptable and would result in loop executing again.
                if (!userInput.equals("y") && !userInput.equals("n"))
                {
                    throw new ImproperAnswerException();
                }
                
                // At this point, only values possible are "y" and "n." 
                // "y" = true, and if input = "y" then true will be stored.
                // If input = "n" then false will be stored.
                userChoice = userInput.equals("y");
                
                keepLooping = false;
            }
            catch (ImproperAnswerException ex)
            {
                System.out.println(ex);
                keepLooping = true;
            }
        } while (keepLooping);
        
        return (userChoice);
    }
}
package eyc.exception;

/**
 * Exception to be thrown if the user is asked to input a number in 
 * a particular range and fails to do so. 
 */ 
public class ImproperRangeException extends Exception
{
    // Instance variables.
    private int low; // Lowest value of range.
    private int high; // Highest value of range.
    
    /**
     * Constructor for an ImproperRangeException.
     * 
     * @param low The lowest value of the range.
     * 
     * @param high The highest value of the range.
     */ 
    public ImproperRangeException(int low, int high)
    {
        this.low = low;
        this.high = high;
    }
    
    @Override
    public String toString()
    {
        return ("Number must be between " + low + " and " + high + ".");
    }
}
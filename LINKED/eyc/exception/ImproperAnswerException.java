package eyc.exception;

/**
 * Exception to be thrown if user is asked a yes-or-no question
 * and responds with something other than "y" (for yes) or "n" (for no).
 */ 
public class ImproperAnswerException extends Exception
{
    @Override
    public String toString()
    {
        return ("Answer must be either \"Y\" or \"N\".");
    }
}
package eyc.exception;

/**
 * Exception to be thrown if the user is asked to input a positive number
 * and fails to do so.
 */ 
public class NonPositiveNumberException extends Exception
{
    @Override
    public String toString()
    {
        return ("Number must be positive.");
    }
}
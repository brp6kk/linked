package eyc.exception;

/**
 * Exception to be thrown when a positive number OR zero is expected, but
 * instead a negative number is received.
 */ 
public class NegativeNumberException extends Exception
{
    @Override
    public String toString()
    {
        return ("Number cannot be negative.");
    }
}
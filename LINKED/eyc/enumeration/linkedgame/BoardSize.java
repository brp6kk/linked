package eyc.enumeration.linkedgame;

/**
 * Enumeration containing values that represent Board sizes.
 */ 
public enum BoardSize
{
    // BoardSize values.
    DEFAULT(6, 7), ALT_1(4, 5), ALT_2(5, 6), ALT_3(7, 8),
    ALT_4(7, 9), ALT_5(7, 10), ALT_6(8, 8);
    
    // Instance variables.
    private int rows;
    private int columns;
    
    // Constructor for each BoardSize value.
    private BoardSize(int rows, int columns)
    {
        this.rows = rows;
        this.columns = columns;
    }
    
    // Accessor - Number of rows.
    public int getRows()
    {
        return (rows);
    }
    
    // Accessor - Number of columns.
    public int getColumns()
    {
        return (columns);
    }
    
    @Override
    public String toString()
    {
        return (rows + " rows by " + columns + " columns");
    }
}
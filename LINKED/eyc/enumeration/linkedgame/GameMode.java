package eyc.enumeration.linkedgame;

import eyc.funcinterface.LinkedGameConstructor;
import eyc.game.linkedgame.*;
import eyc.game.linkedgame.modes.*;

/**
 * Enumeration containing values that represent game modes.
 */ 
public enum GameMode
{
    // GameMode values.
    CLASSIC(Classic::new, "Classic"), PLUSONE(PlusOne::new, "Plus One"),
    ZIGZAG(ZigZag::new, "ZigZag"), POPOUT(PopOut::new, "Pop-Out"),
    TWISTED(Twisted::new, "Twisted"), GRAVITY(Gravity::new, "Gravity");
    
    // Instance variables.
    private LinkedGameConstructor constructor;
    private String name;
    
    // Constructor for each GameMode value.
    private GameMode(LinkedGameConstructor constructor, String name)
    {
        this.constructor = constructor;
        this.name = name;
    }
    
    // Accessor - Reference to the game mode's constructor.
    public LinkedGameConstructor getConstructor()
    {
        return (constructor);
    }
    
    @Override
    public String toString()
    {
        return (name);
    }
}
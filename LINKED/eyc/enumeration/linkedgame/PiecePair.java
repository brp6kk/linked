package eyc.enumeration.linkedgame;

/**
 * Enumeration containing values that represent piece pairs.
 */ 
public enum PiecePair
{
    // PiecePair values.
    DEFAULT('X', 'O'), ALT_1('*', '$'), ALT_2('~', '@'),
    ALT_3('?', '!'), ALT_4('Y', 'M'), ALT_5('&', '=');
    
    // Instance variables.
    private char p1;
    private char p2;
    
    // Constructor for each PiecePair value.
    private PiecePair(char p1, char p2)
    {
        this.p1 = p1;
        this.p2 = p2;
    }
    
    // Accessor - Player 1's piece.
    public char getPlayer1()
    {
        return (p1);
    }
    
    // Accessor - Player 2's piece.
    public char getPlayer2()
    {
        return (p2);
    }
    
    @Override
    public String toString()
    {
        return (p1 + " vs. " + p2);
    }
}
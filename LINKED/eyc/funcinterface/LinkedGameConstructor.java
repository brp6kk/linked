package eyc.funcinterface;

import eyc.game.components.linkedgame.*;
import eyc.game.linkedgame.*;
import eyc.exception.*;

public interface LinkedGameConstructor
{
    public Game create(int rows, int columns, Player[] players) 
    throws NonPositiveNumberException;
}
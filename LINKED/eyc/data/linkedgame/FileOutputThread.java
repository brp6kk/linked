package eyc.data.linkedgame;

import eyc.game.components.linkedgame.*;

/**
 * Thread that writes over a data file.  If a Player's data already exists
 * in the file, their number of wins is replaced with the number sent to
 * this thread.  If a Player's data does not exist in the file, their
 * data is added to the end.
 */ 
public class FileOutputThread implements FileThread
{
    // Instance variables.
    private Thread thread;
    private Player player;
    
    /**
     * Constructor for a FileOutputThread.
     * 
     * @param name The name of the Player.
     * 
     * @param wins The number of wins of the Player.
     */ 
    public FileOutputThread(Player player)
    {
        this.player = player;
        thread = new Thread(this);
        thread.setPriority(Thread.NORM_PRIORITY - 2);
        this.start();
    }
    
    /**
     * Code that should execute when a FileOutputThread runs.
     */ 
    @Override
    public void run()
    {
        DataIO.writeNumWins(player.getName(), player.getNumWins());
    }
    
    /**
     * Causes run() to start.
     */ 
    public void start()
    {
        thread.start();
    }
    
    /**
     * Accesses the actual Thread associated with this FileOutputThread.
     * 
     * @return The Thread object handling this FileOutputThread.
     */ 
    public Thread getThread()
    {
        return (thread);
    }
}
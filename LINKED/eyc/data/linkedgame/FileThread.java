package eyc.data.linkedgame;

/**
 * Interface outlining what should be implemented in a Thread that
 * handles files.
 */ 
public interface FileThread extends Runnable
{
    public void start();
    public Thread getThread();
}
package eyc.data.linkedgame;

import eyc.game.components.linkedgame.*;
import eyc.exception.*;

/**
 * Thread that finds a Player in the data file and determines their
 * number of previous wins.  Because of object references, no data needs
 * to be returned.  After this thread finishes execution, their number of
 * previous wins will be stored in the relevant Integer object.
 */
public class FileInputThread implements FileThread
{
    // Instance variables.
    private Thread thread;
    private Player player;
    
    /**
     * Constructor for a FileInputThread.
     * 
     * @param name The name of the Player.
     * 
     * @param wins The number of wins of the Player.
     */ 
    public FileInputThread(Player player)
    {
        this.player = player;
        thread = new Thread(this);
        thread.setPriority(Thread.NORM_PRIORITY + 2);
        this.start();
    }
    
    /**
     * Code that should execute when a FileInputThread runs.
     */ 
    @Override
    public void run()
    {
        try
        {
            int wins = DataIO.readNumWins(player.getName());
            
            if (wins < 0) // Number of wins cannot be negative.
            {
                throw new NegativeNumberException();
            }
            
            player.setNumWins(wins);
        }
        catch (NegativeNumberException ex)
        {
            // The following will never throw an exception, but must
            // be enclosed in a try block anyways to be safe.
            try
            {
                player.setNumWins(0);
            }
            catch (NegativeNumberException ex2) {}
        }
    }
    
    /**
     * Causes run() to start.
     */ 
    public void start()
    {
        thread.start();
    }
    
    /**
     * Accesses the actual Thread associated with this FileInputThread.
     * 
     * @return The Thread object handling this FileInputThread.
     */ 
    public Thread getThread()
    {
        return (thread);
    }
}
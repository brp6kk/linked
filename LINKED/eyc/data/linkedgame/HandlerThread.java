package eyc.data.linkedgame;

import java.util.*;

/**
 * Thread that goes through all threads in an ArrayList and removes them
 * when they finish running.
 */ 
public class HandlerThread implements Runnable
{
    // Instance variables.
    private Thread thread;
    private ArrayList<FileThread> threadList;
    private boolean stopped; // run() control.
    
    /**
     * Constructor for a HandlerThread.
     * 
     * @param threadList A reference to a list of threads.
     */ 
    public HandlerThread(ArrayList<FileThread> threadList)
    {
        this.threadList = threadList;
        stopped = false; // Thread should be running by default.
        thread = new Thread(this);
        this.start();
    }
    
    /**
     * Code that executes when a HandlerThread runs.
     */ 
    @Override
    public void run()
    {
        // Infinite loop (though technically not infinite because of the
        // break statement).
        while (true)
        {
            // Loops through all threads in the thread list and removes the
            // ones that are done executing.
            for (int index = 0; index < threadList.size(); index++)
            {
                if (!threadList.get(index).getThread().isAlive())
                {
                    threadList.remove(index);
                    index--; // Ensures no threads are skipped due to numbering.
                }
            }
            
            // Checks to see if the thread should stop running.
            synchronized (this)
            {
                if (stopped)
                {
                    break;
                }
            }
        }
    }
    
    /**
     * Causes run() to start.
     */ 
    public void start()
    {
        thread.start();
    }
    
    /**
     * Changes the flag variable to stop the run() method.
     */ 
    public synchronized void stop()
    {
        stopped = true;
    }
    
    /**
     * Accesses the actual Thread associated with this HandlerThread.
     */ 
    public Thread getThread()
    {
        return (thread);
    }
}
package eyc.data.linkedgame;

import java.util.*;
import java.io.*;

/**
 * Methods that handle Linked's file Input/Output.
 */ 
public class DataIO
{
    // private final static String FILE_NAME = "eyc/data/linkedgame/testdata.txt";
    private final static String FILE_NAME = "eyc/data/linkedgame/playerdata.txt";
    private final static String DELIM = ",";
    
    
    /**
     * Searches for a name within the data file and returns the number of
     * wins associated with it.
     * 
     * @param name The name to search for.
     * 
     * @return The number of wins associated with the name, or zero if the
     * player doesn't exist OR if any errors occur.
     */ 
    static synchronized int readNumWins(String name)
    {
        // Variable declaration.
        Scanner reader;
        int wins = 0; // Default number of wins.
        boolean nameFound = false; // Loop control variable.
        
        // Tries to open up the data file.
        // If the file does not exist, then the player will just start with
        // zero wins.
        try
        {
            reader = new Scanner(new File(FILE_NAME));
        }
        catch (FileNotFoundException ex)
        {
            // System.out.println(ex);
            return (wins); // wins should be 0.
        }
        
        // Loops through the entire file until/unless the name is found.
        while (reader.hasNext() && !nameFound)
        {
            // Splits the line based on the delimiter of the file.
            String[] line = reader.nextLine().split(DELIM);
            
            // If the data is formatted correctly and the name matches...
            if (line.length == 2 && line[0].equals(name))
            {
                // ... try converting the second value to an integer.
                // If the conversion succeeds, this number is the player's 
                // previous number of wins.  Otherwise, the player will just
                // start with zero wins.
                try
                {
                    wins = Integer.parseInt(line[1]);
                }
                catch (NumberFormatException ex)
                {
                    // System.out.println(ex);
                    wins = 0;
                }
                finally // No matter what happened, the name was found.
                {
                    nameFound = true;
                }
            }
        }
        
        reader.close();
        
        return (wins);
    }
    
    
    /**
     * Updates the data file with a player's stats.
     * 
     * @param The name of the Player.
     * 
     * @param wins The number of wins for the Player.
     */ 
    static synchronized void writeNumWins(String name, int wins)
    {
        // Variable declarations.
        Scanner reader;
        boolean nameFound = false;
        String line;
        String[] splitLine;
        ArrayList<String> fileLines = new ArrayList<String>();
        
        // Tries to open up the data file.
        // If the file does not exist, writing does not occur and the method
        // is exited.
        try
        {
            reader = new Scanner(new File(FILE_NAME));
        }
        catch (FileNotFoundException ex)
        {
            // System.out.println(ex);
            return;
        }
        
        // Loops through the entire file.
        while (reader.hasNext())
        {
            // Gets and splits line.
            line = reader.nextLine();
            splitLine = line.split(DELIM);
            
            // Line has to be properly formatted.
            // If it is, and the name is identical to the parameter,
            // The line is altered with the new number of wins.
            if (splitLine.length == 2 && splitLine[0].equals(name))
            {
                line = splitLine[0] + DELIM + wins;
                nameFound = true;
            }
            
            // No matter what, the line is added to the ArrayList.
            fileLines.add(line);
        }
        
        reader.close();
        
        // If the name was not found, the data is added to the end of
        // the ArrayList.
        if (!nameFound)
        {
            line = name + DELIM + wins;
            fileLines.add(line);
        }
        
        // Tries to write to file only if the file can be found.
        try
        {
            writeToFile(fileLines);
        }
        catch (FileNotFoundException ex) {}
    }
    
    
    /**
     * Writes all lines from a list to a file.
     * 
     * @param lines The data to write.
     * 
     * @exception Thrown when the data file cannot be opened.
     */ 
    static void writeToFile(ArrayList<String> lines) 
    throws FileNotFoundException
    {
        // Variable declaration.
        PrintWriter writer;
        
        // Tries to open a new file to write to.
        // If an error occurs, this method is exited.
        try
        {
            writer = new PrintWriter(new File(FILE_NAME));
        }
        catch (FileNotFoundException ex)
        {
            // System.out.println(ex);
            throw (ex);
        }
        
        // Writes each line to the file.
        for (String line : lines)
        {
            writer.println(line);
        }
        
        writer.close();
    }
    
    
    /**
     * Finds highest scorer(s) from the data file and displays this
     * information.  If there are multiple people tied for having the
     * highest number of wins, all of their names are displayed.
     */ 
    public synchronized static void showHighestScorer()
    {
        // Instance variables.
        Scanner reader;
        int highScore = -1; // Actual high scores can never be negative.
        ArrayList<String> names = new ArrayList<String>();
        
        // Tries to open up the data file.
        // If the data file does not exist, no high scorer data is shown.
        try 
        {
            reader = new Scanner(new File(FILE_NAME));
        }
        catch (FileNotFoundException ex)
        {
            // System.out.println(ex);
            return;
        }
        
        // Loops through the entire file.
        while (reader.hasNext())
        {
            // Gets and splits line.
            String[] line = reader.nextLine().split(DELIM);
            
            // Only lines of the proper length should be considered.
            if (line.length == 2)
            {
                // Tries converting the second value to an integer. 
                try
                {
                    int wins = Integer.parseInt(line[1]);
                    
                    if (wins == highScore) // Tie for current highest score.
                    {
                        // Add this name to the list of tied scorers.
                        names.add(line[0]);
                    }
                    else if (wins > highScore) // New highest score.
                    {
                        // Clears name list and restarts it with this
                        // highest scorer's name.  High score holder is
                        // updated appropriately.
                        names.clear();
                        names.add(line[0]);
                        highScore = wins;
                    }
                }
                catch (NumberFormatException ex)
                {
                    // System.out.println(ex);
                }
            }
        }
        
        reader.close();
        
        // If the high score is still -1 by this point, either the
        // data file was empty or the data was improperly formatted.
        // Either way, no information should be displayed.
        if (highScore != -1)
        {
            // Label.
            System.out.println("Current highest number of wins: ");
            
            System.out.print(highScore + " wins - ");
            
            // Names are seperated by commas appropriately.
            // "Name, Name, Name, ..., Name"
            // No comma with the last name, OR no comma if there is a 
            // single name.
            int index;
            for (index = 0; index < names.size() - 1; index++)
            {
                System.out.print(names.get(index) + ", ");
            }
            System.out.println(names.get(index));
        }
    }
}
package eyc.program;

import eyc.game.components.linkedgame.*;
import eyc.game.linkedgame.*;
import eyc.game.linkedgame.modes.*;
import eyc.input.*;
import eyc.enumeration.linkedgame.*;
import eyc.funcinterface.LinkedGameConstructor;
import eyc.data.linkedgame.*;
import eyc.exception.*;
import java.util.*;

/* Project: Linked 2.0
 * Name: Eric Chang
 * Date: 07/08/2019
 * Description: Linked is a strategic connection game where players
 * "drop" pieces into a column in a grid, which then fall to the 
 * lowest unoccupied space.  This program builds off of this concept 
 * by providing several different variants and board sizes.
 * Notes: 
 * * Linked 1.0 was my final project for my AP Computer Science A class, which
 *   I took during the 2018-2019 school year (senior year at HCHS).  After
 *   taking the class, I learned more about the Java programming language
 *   (using "Java A Beginner's Guide Seventh Edition").  Now that I know more
 *   about the language, I'm implementing what I learned into Linked 2.0.
 * * Files included in this project:
 * * * eyc.enumeration.linkedgame.*;
 * * * eyc.funcinterface.LinkedGameConstructor.java;
 * * * eyc.game.components.linkedgame.*;
 * * * eyc.game.linkedgame.*;
 * * * eyc.input.NumIn.java;
 * * * eyc.input.StrIn.java;
 * * * eyc.program.LinkedRunner.java;
 * * * eyc.test.LinkedTester.java;
 * * * eyc.data.linkedgame.*;
 * * * eyc.exception.*;
 * * Changes made:
 * * * Separated files into packages (06/28/2019)
 * * * Created enumerations for BoardSize, PiecePair, and GameMode (06/30/2019)
 * * * Cleaned up LinkedRunner.java (6/30/2019)
 * * * * Added getComponent()
 * * * * Removed getGameMode(), getBoardSize(), getPiecePair(), createGame()
 * * * * Renamed setUp() to setUpNew().
 * * * * Other alterations to implement getComponent() and handle removed methods.
 * * * Updated Player class so that a Player's number of wins can be changed
 * *   once and only once (necessary for thread implementation) (07/05/2019)
 * * * Added threads (and all other code in eyc.data.linkedgame) to allow for
 * *   data to be saved.  This data is about how many times each Player has
 * *   won a game. (07/05/2019)
 * * * Added code to LinkedRunner.java to implement the new threads (07/05/2019)
 * * * * Added ArrayList of FileThreads and a HandlerThread.
 * * * * Added code to start new FileInputThreads and FileOutputThreads.
 * * * * Added threadCleanup() method.
 * * * Created ImproperRangeException, ImproperAnswerException, and 
 * *   NonPositiveNumberException (eyc.exception.*;) (07/07/2019)
 * * * Implemented the new exceptions appropriately (07/07/2019)
 * * * Altered access modifiers appropriately (07/08/2019)
 */
public class LinkedRunner
{
    public static void main(String[] args)
    {
        // Variable declarations.
        Scanner input = new Scanner(System.in);
        boolean quit = false;
        boolean rematch = false;
        boolean win = false;
        Game game;
        ArrayList<FileThread> threadList = new ArrayList<FileThread>();
        HandlerThread handler = new HandlerThread(threadList);
        
        // Title of game and highest scorer are shown.
        System.out.println("Welcome to LINKED, " + 
                           "the strategic connection game!\n\n\n\n\n");
        DataIO.showHighestScorer();
        
        // Loops until the user wishes to exit the program.
        do
        {
            // Sets up a new game with new players.
            try
            {
                game = setUpNew(input, threadList);
            }
            catch (NonPositiveNumberException ex)
            {
                System.out.println("Critical error (code 1). " + 
                                   "Please see the user manual for " +
                                   "instructions on contacting the " +
                                   "developers.\nTerminating program.");
                threadCleanup(handler, threadList);
                return;
            }
            // Loops until the two players no longer wish to play.
            do
            {
                // A single game is played, and if the two want to play
                // against each other again, they can do so.
                win = game.play(input);
                rematch = endScreen(game, win, input);
                if (rematch)
                {
                    try
                    {
                        game = setUpRematch(game.getPlayers(), input);
                    }
                    catch (NonPositiveNumberException ex)
                    {
                        System.out.println("Critical error (code 1). " + 
                                   "Please see the user manual for " +
                                   "instructions on contacting the " +
                                   "developers.\nTerminating program.");
                        threadCleanup(handler, threadList);
                        return;
                    }
                }
            } while (rematch);
            
            // Creates new Threads to save Player information to the data file.
            for (Player player : game.getPlayers())
            {
                threadList.add(new FileOutputThread(player));
            }
            
            // Loop will execute again if two different players wish to play.
            quit = StrIn.getAnswer("Do you want to exit this program?", input);
        } while (!quit);
        
        System.out.println("\n\nThank you for enjoying LINKED!");
        
        threadCleanup(handler, threadList);
    }
    
    /**
     * Obtains a component of a game.
     * 
     * @param values All of the possible values of the component.
     * 
     * @param name The String equivalent of the datatype of values.
     * 
     * @param input Scanner to read in user input.
     * 
     * @return The value of the component.
     */ 
    private static <T extends Enum> T getComponent(T[] values, String name, 
                                                  Scanner input)
    {
        // Prints the prompt for the user, consisting of all possible values
        // of the component.
        System.out.println("Which " + name + " should be used?");
        for (int index = 0; index < values.length; index++)
        {
            System.out.println((index + 1) + ". " + values[index]);
        }
        
        // Obtains number corresponding to component.
        // Number is one greater than actual index of component, so it
        // is decremented by 1.
        int componentIndex = NumIn.getInt(1, values.length, 
                                          ("Enter " + name + ": "), input);
        componentIndex--;
        
        // Gets the actual component and returns it.
        T component = values[componentIndex];
        return(component);
    }
    
    /**
     * Creates two players, each with unique names and pieces.
     * 
     * @param input Scanner to read in user input.
     * 
     * @param threadList A list of FileThreads, which will be added to after
     * the Players are created.
     * 
     * @return The two players to play the game.
     */
    private static Player[] createPlayers(Scanner input, 
                                         ArrayList<FileThread> threadList)
    {
        // Variable declarations.
        Player[] players = new Player[2];
        String[] names = new String[2];
        
        // Obtains the names of the two players.
        for (int index = 0; index < 2; index++)
        {
            System.out.print("Enter PLAYER " + ((int)index+1) + "'s name: ");
            names[index] = input.nextLine();
        }
        
        // Gets the pieces for the players to use.
        PiecePair pair = getComponent(PiecePair.values(), "PIECE PAIR", input);
        
        // Creates the two players from the given names and piece pair.
        players[0] = new Player(names[0], pair.getPlayer1());
        players[1] = new Player(names[1], pair.getPlayer2());
        
        // Creates new Threads to obtain Player win data for each new Player.
        for (Player player : players)
        {
            threadList.add(new FileInputThread(player));
        }
        
        return (players);
    }
    
    /**
     * Sets up a brand new game based on the user's desires.
     * 
     * @param input Scanner to read in user input.
     * 
     * @param threadList A list of FileThreads, which will be used when the
     * Players are created.
     * 
     * @return The fully set-up game.
     * 
     * @exception Thrown if any of the board dimensions are nonpositive.
     */
    private static Game setUpNew(Scanner input, ArrayList<FileThread> threadList)
    throws NonPositiveNumberException
    {
        // All System.out.println's are used to separate output.
        // Desired game mode, desired board size, and the players
        // are obtained.  This information is used to create a 
        // new game.
        System.out.println("\n");
        GameMode mode = getComponent(GameMode.values(), "GAME MODE", input);
        System.out.println();
        BoardSize size = getComponent(BoardSize.values(), "BOARD SIZE", input);
        System.out.println();
        Player[] players = createPlayers(input, threadList);
        Game game;
        try
        {
            game = mode.getConstructor().create(size.getRows(), 
                                                size.getColumns(), players);
        }
        catch (NonPositiveNumberException ex)
        {
            throw (ex);
        }
        
        return (game);
    }
    
    /**
     * Sets up a new game using already established players.
     * 
     * @param players The players to use for the game.
     * 
     * @param input Scanner to read in user input.
     * 
     * @return The fully set-up game.
     * 
     * @exception Thrown if any of the board dimensions are nonpositive.
     */
    private static Game setUpRematch(Player[] players, Scanner input)
    throws NonPositiveNumberException
    {
        // All System.out.println's are used to separate output.
        // Desired game mode and board sizes are obtained.  This 
        // information, along with the already established players,
        // is used to create a new game.
        System.out.println("\n");
        GameMode mode = getComponent(GameMode.values(), "GAME MODE", input);
        System.out.println();
        BoardSize size = getComponent(BoardSize.values(), "BOARD SIZE", input);
        System.out.println();
        Game game;
        
        try
        {
            game = mode.getConstructor().create(size.getRows(), 
                                                size.getColumns(), players);
        }
        catch (NonPositiveNumberException ex)
        {
            throw (ex);
        }
        
        return (game);
    }
    
    /**
     * Displays stats on the players after a game occurs and determines
     * if a rematch is desired.
     * 
     * @param game The recently-finished game.
     * 
     * @param win True if the game resulted in a win; false if the game
     * resulted in a tie.
     * 
     * @param input Scanner to read in user input.
     * 
     * @return True if a rematch is desired; false otherwise.
     */
    private static boolean endScreen(Game game, boolean win, Scanner input)
    {
        // Draws the final state of the board.
        System.out.println();
        game.drawBoard();
        System.out.println();
        
        // Different message is displayed depending on if there was
        // a clear winner or if there was a tie.
        if (win)
        {
            System.out.println("Congratulations to the winner!");
        }
        else
        {
            System.out.println("Game ended in a tie.");
        }
        
        // Displays total number of wins for both players.
        for (Player player : game.getPlayers())
        {
            System.out.println(player.getName() + ": " + 
                               player.getNumWins() + " wins.");
        }
        
        System.out.println();
        
        // Determines if a rematch is desired.
        boolean answer = StrIn.getAnswer("Rematch?", input);
        return (answer);
    }
    
    /**
     * Deals with final Thread activities before the whole program ends.
     * 
     * @param handler The HandlerThread that keeps track of the threadList.
     * 
     * @param threadList A list of all FileThreads currently running.
     */ 
    private static void threadCleanup(HandlerThread handler, 
                                     ArrayList<FileThread> threadList)
    {
        // Stops handler.
        handler.stop();
        
        try
        {
            // Any remaining FileThreads need to finish executing.
            for (FileThread thread : threadList)
            {
                thread.getThread().join();
            }
        }
        catch(InterruptedException ex) {}
    }
}
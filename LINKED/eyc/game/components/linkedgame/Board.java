package eyc.game.components.linkedgame;

import eyc.exception.*;

/**
 * Class that represents a board, which is used to play a game.
 */
public class Board
{
    // Instance variables.
    private int numRows;
    private int numColumns;
    private char[][] grid;
    private static final char EMPTY = 0;
    
    
    /********** Constructors **********/
    
    /**
     * Parameterized constructor for the Board class.
     * 
     * @param numRows The number of rows for the Board.
     * 
     * @param numColumns The number of columns for the Board.
     * 
     * @exception Thrown when either numRows or numColumns is nonpositive.
     */
    public Board(int numRows, int numColumns) 
    throws NonPositiveNumberException
    {
        if (!(numRows > 0) || !(numColumns > 0))
        {
            throw new NonPositiveNumberException();
        }
         
        this.numRows = numRows;
        this.numColumns = numColumns;
        
        // 2-D array representing the board has (numRows) rows and
        // (numColumns) columns.
        this.grid = new char[numRows][numColumns];
        
        // Fills each cell in grid with the default (empty) value.
        for (int row = 0; row < grid.length; row++)
        {
            for (int cell = 0; cell < grid[row].length; cell++)
            {
                this.grid[row][cell] = EMPTY;
            }
        }
    }
    
    
    /********** Accessors/Getters **********/
    
    /**
     * Gets the number of rows in the Board.
     * 
     * @return The Board's rows.
     */
    public int getNumRows()
    {
        return (numRows);
    }
    
    /**
     * Gets the number of columns in the Board.
     * 
     * @return The Board's columns.
     */
    public int getNumColumns()
    {
        return (numColumns);
    }
    
    /**
     * Gets the piece at a particular spot on the Board.
     * 
     * @param row The row of the piece.
     * 
     * @param column The column of the piece.
     * 
     * @return The piece at the indicated cell, or empty if the
     * cell does not exist.
     */
    public char getPieceAt(int row, int column)
    {
        // Can't return a piece if the row does not exist or
        // if the column does not exist.
        if (!validLocation(row, column))
        {
            return (EMPTY);
        }
        
        return (this.grid[row][column]);
    }
    
    /**
     * Gets the value that represents an empty space.
     * 
     * @return The "empty" value.
     */
    public char emptyValue()
    {
        return (EMPTY);
    }
    
    
    /********** Mutators/Setters **********/
    
    /**
     * Adds a piece to a spot on the Board.  No piece is placed if
     * the cell is already full or if the cell doesn't exist.
     * 
     * @param piece The piece to add to the Board.
     * 
     * @param row The row of the cell to place the piece at.
     * 
     * @param column The column of the cell to place the piece at.
     */
    public void placePiece(char piece, int row, int column)
    {
        // Can't place piece if the spot indicated already
        // contains a piece, if the row does not exist, or
        // if the column does not exist.
        if (!validLocation(row, column) || this.grid[row][column] != EMPTY)
        {
            return;
        }
        
        grid[row][column] = piece;
    }
    
    /**
     * Removes a piece from a spot on the Board.  No piece is removed
     * if the cell does not exist.
     * 
     * @param row The row of the cell to remove the piece from.
     * 
     * @param column The column of the cell to remove the piece from.
     */
    public void removePiece(int row, int column)
    {
        // Can't remove piece if the row does not exist or
        // if the column does not exist.
        if (!validLocation(row, column))
        {
            return;
        }
        
        grid[row][column] = EMPTY;
    }
    
    
    /********** Other Methods **********/
    
    /**
     * By default, no row numbers should be drawn on the board.
     * By creating this method, this doesn't need to be specified
     * every time that a board needs to be drawn.
     */
    public void drawBoard()
    {
        drawBoard(false);
    }
    
    /**
     * Draws the current state of the Board.
     * 
     * @param drawRowNumbers True if the Board requires row
     * numbers be drawn; false otherwise.
     */
    public void drawBoard(boolean drawRowNumbers)
    {
        // Variable declarations.
        int numLines = 2 * this.numRows + 1;
        int currentRow = 0;
        
        // Loops until the entire board is drawn.
        for (int line = 0; line < numLines; line++)
        {
            // Even index = draw a line that separates cells.
            if (line % 2 == 0)
            {
                // Loops through the total number of columns, 
                // drawing a line for each one.
                for (int column = 0; column < this.numColumns; column++)
                {
                    System.out.print("+---");
                }
                
                // Additional "+" added to finish off the line.
                System.out.print("+");
            }
            // Odd index = draw cells.
            else
            {
                // Loops through the total number of columns.
                for (int column = 0; column < this.numColumns; column++)
                {
                    // First part of cell.
                    System.out.print("| ");
                    
                    // Prints out piece if the cell is not empty;
                    // prints whitespace if the cell is empty.
                    if (this.grid[currentRow][column] != EMPTY)
                    {
                        System.out.print(this.grid[currentRow][column]);
                    }
                    else
                    {
                        System.out.print(" ");
                    }
                    
                    // Last part of cell.
                    System.out.print(" ");
                }
                // Additional "|" added to finish off the line.
                System.out.print("|");
                
                // Draws row numbers only if indicated to do so.
                if (drawRowNumbers)
                {
                    System.out.print(" " + currentRow);
                }
                
                currentRow++;
            }
            
            // Adds newline to separate lines.
            System.out.println();
        }
        
        // Prints each column number.
        for (int index = 0; index < this.numColumns; index++)
        {
            System.out.print("  " + index + " ");
        }
        System.out.println();
    }
    
    /**
     * Determines if every cell in the Board has a piece.
     * 
     * @return True if there are no empty cells; false otherwise.
     */
    public boolean boardIsFull()
    {
        // Loops through all rows in the grid.
        for (int row = 0; row < this.numRows; row++)
        {
            // Loops through all cells in the row.
            for (int column = 0; column < this.numColumns; column++)
            {
                // If the cell is empty, the board is not full.
                if (this.grid[row][column] == EMPTY)
                {
                    return (false);
                }
            }
        }
        
        // No empty spaces were found.
        return (true);
    }
    
    /**
     * Determines if a piece exists in a given row and column.
     * 
     * @param row The row of the cell to check.
     * 
     * @param column The column of the cell to check.
     * 
     * @return True if there is a piece in the given spot; 
     * false if there is no piece there.
     */
    public boolean pieceExists(int row, int column)
    {
        // Piece doesn't exist if the row does not exist or
        // if the column does not exist.
        if (!validLocation(row, column) || grid[row][column] == EMPTY)
        {
            return (false);
        }
        else
        {
            return (true);
        } 
    }
    
    /**
     * Determines if a location exists on the Board.
     * 
     * @param row The row of the cell to check.
     * 
     * @param column The column of the cell to check.
     * 
     * @return True if the Board contains the given coordinate;
     * false if the coordinate is not on the Board.
     */
    public boolean validLocation(int row, int column)
    {
        // Row has to be somewhere between 0 (inclusive) and the
        // number of rows (exclusive).
        // Column as to be somewhere between 0 (inclusive) and
        // the number of columns (exclusive).
        if ( (row >= 0 && row < numRows) &&
             (column >= 0 && column < numColumns) )
        {
            return (true);
        }
        else
        {
            return (false);
        }
    }
}
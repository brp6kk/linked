package eyc.game.components.linkedgame;

import eyc.exception.*;

/**
 * Class representing a Player of a Game.
 */
public class Player
{
    // Instance variables.
    private String name;
    private char piece;
    private int numWins;
    private boolean numWinsSpecified;
    
    
    /********** Constructors **********/
    
    /**
     * Parameterized constructor for the Player class.
     * 
     * @param name The name of the Player.
     * 
     * @param piece The Player's piece.
     */
    public Player (String name, char piece)
    {
        this.name = name;
        this.piece = piece;
        this.numWins = 0;
        this.numWinsSpecified = false;
    }
    
    /**
     * Parameterized constructor for the Player class.
     * 
     * @param name The name of the Player.
     * 
     * @param piece The Player's piece.
     * 
     * @param numWins The number of times the Player has 
     * previously won a game.
     * 
     * @exception Thrown when numWins is negative.
     */
    public Player (String name, char piece, int numWins)
    throws NegativeNumberException
    {
        if (numWins < 0) // Number of wins cannot be negative.
        {
            throw new NegativeNumberException();
        }
        
        this.name = name;
        this.piece = piece;
        this.numWins = numWins;
        this.numWinsSpecified = true;
    }
    
    
    /********** Accessors/Getters **********/
    
    /**
     * Gets the name of the Player.
     * 
     * @return The Player's name.
     */
    public String getName()
    {
        return (name);
    }
    
    /**
     * Gets the piece of the Player.
     * 
     * @return The Player's piece.
     */
    public char getPiece()
    {
        return (piece);
    }
    
    /**
     * Gets the number of wins for the Player.
     * 
     * @return The Player's number of wins.
     */
    public int getNumWins()
    {
        return (numWins);
    }
    
    
    /********** Mutators/Setters **********/
    
    /**
     * Adds a win to the Player's count of wins.
     */
    public void addWin()
    {
        numWins++;
    }
    
    /**
     * Changes the Player's number of wins ONLY IF the number of wins has
     * never been specified before.
     * 
     * @param wins The number of wins of this Player.
     * 
     * @exception Thrown when wins is negative.
     */ 
    public void setNumWins(int wins)
    throws NegativeNumberException
    {
        if (wins < 0) // Number of wins cannot be negative.
        {
            throw new NegativeNumberException();
        }
        
        if (!numWinsSpecified)
        {
            // In theory, numWins should be zero.
            // If, though, the number of wins isn't found until after
            // a/several games have been played, these games need to be
            // accounted for.
            this.numWins = wins + numWins;
            numWinsSpecified = true;
        }
    }
    
    
    /********** Other Methods **********/
    
    /**
     * Creates a String with descriptive information about the Player.
     * 
     * @return The Player info.
     */
    @Override
    public String toString()
    {
        // Creates the description of this player, using
        // all instance variables.
        String message = "The player named ";
        message += name;
        message += " is using the piece ";
        message += piece;
        message += ".  ";
        message += name;
        message += " has won ";
        message += numWins;
        message += " games.";
        
        return (message);
    }
}
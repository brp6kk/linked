package eyc.game.linkedgame;

import eyc.game.components.linkedgame.*;
import eyc.input.*;
import eyc.exception.*;
import java.util.*;

/**
 * Class to build various Games.
 */
public abstract class Game
{
    // Instance variables.
    private Board board;
    private Player[] players;
    int winLength;
    
    
    /********** Constructors **********/
    
    /** 
     * Parameterized constructor for the Game class.
     * 
     * @param boardRows The number of rows for the Game's Board.
     * 
     * @param boardColumns The number of columns for the
     * Game's Board.
     * 
     * @param players An array containing the Players of the Game.
     */
    public Game (int boardRows, int boardColumns, Player[] players)
    throws NonPositiveNumberException
    {
        try
        {
            this.board = new Board(boardRows, boardColumns);
            this.players = players;
            this.winLength = 4; // Most games requires 4 in a row to win.
        }
        catch (NonPositiveNumberException ex)
        {
            throw (ex);
        }
    }
    
    /** 
     * Parameterized constructor for the Game class.
     * 
     * @param boardRows The number of rows for the Game's Board.
     * 
     * @param boardColumns The number of columns for the
     * Game's Board.
     * 
     * @param players An array containing the Players of the Game.
     * 
     * @param winLength The number of pieces in a row required
     * to win a Game.
     */
    public Game (int boardRows, int boardColumns, Player[] players, int winLength)
    throws NonPositiveNumberException
    {
        try
        {
            this.board = new Board(boardRows, boardColumns);
            this.players = players;
            this.winLength = winLength;
        }
        catch (NonPositiveNumberException ex)
        {
            throw (ex);
        }
    }
    
    
    /********** Accessors/Getters **********/
    
    /**
     * Gets the Board associated with this Game.
     * 
     * @return The Game's Board.
     */
    public Board getBoard()
    {
        return (board);
    }
    
    /**
     * Gets the Players who are playing this Game.
     * 
     * @return The Game's Players.
     */
    public Player[] getPlayers()
    {
        return (players);
    }
    
    
    /********** Mutators/Setters **********/
    
    // No setters necessary for this class.
    
    
    /********** Other Methods **********/
    
    /**
     * Plays a single Game.
     * 
     * @param input Scanner to read in user input.
     * 
     * @return True if one player won; false if game ended in a tie.
     */
    public boolean play(Scanner input)
    {
        // Variable declarations.
        boolean gameOver = false;
        boolean playerWon = false;
        
        // Loops until the game is over.
        do
        {
            // Loops through each player, or until the game is over.
            for (int index = 0; index < players.length && !gameOver; index++)
            {
                // Separates moves.
                for (int loop = 0; loop < 20; loop++)
                {
                    System.out.println();
                }
                    
                // Redraws board at start of each move
                drawBoard();
                
                // Labels whose turn it is.
                System.out.println("Player " + players[index].getName() +
                                   " (" + players[index].getPiece() + 
                                   "), it's your turn!");
                
                // Current player makes a move.
                makeMove(players[index].getPiece(), input);
                
                // If the board is full, no more moves can be made.
                // The game is over.
                if (checkForTie())
                {
                    playerWon = false;
                    gameOver = true;
                }
                // If there is a winning move, that means that the
                // current player is the winner.  The game is over.
                else if (checkForWin())
                {
                    players[index].addWin();
                    playerWon = true;
                    gameOver = true;
                }
            }
        } while (!gameOver);
        
        return (playerWon);
    }
    
    /**
     * Draws board.
     */
    public void drawBoard()
    {
        board.drawBoard();
    }
    
    /**
     * Makes a single move.
     * 
     * @param piece The piece to be placed in this move.
     * 
     * @param input Scanner to read in user input.
     */
    protected void makeMove(char piece, Scanner input)
    {
        // Variable declarations.
        boolean moveMade = false;
        int dropColumn;
        String prompt = "Enter the column to drop a piece into: ";
        
        // Loops until a valid move is entered.
        do
        {
            // Gets column that user wants to drop piece into.
            dropColumn = NumIn.getInt(0, board.getNumColumns() - 1, prompt, input);
            
            // If move is legal, drops the piece.
            if (moveIsLegal(dropColumn))
            {
                // Variable declarations.
                int dropRow = 0;
                boolean canDrop = false;
                
                // Loops until the lowest row is found.
                while (!canDrop)
                {
                    // No piece is in this row; check next row.
                    // Stops checking rows if the bottom of the board
                    // is reached.
                    if (!board.pieceExists(dropRow, dropColumn) && 
                        !(dropRow >= board.getNumRows()))
                    {
                        dropRow++;
                    }
                    // Piece is in this row; new piece will be placed
                    // in previous row.
                    else
                    {
                        dropRow--;
                        canDrop = true;
                    }
                }
                
                // Adds piece to proper row.
                board.placePiece(piece, dropRow, dropColumn);
                moveMade = true;
            }
            // If move is not legal, player is alerted and loop
            // executes once more.
            else
            {
                System.out.println("Column is full, select another.");
                moveMade = false;
            }
        } while (!moveMade);
    }
    
    /**
     * Determines if a piece can be placed in a column.
     * 
     * @param column The column to be checked.
     * 
     * @return True if the piece can be placed; false otherwise.
     */
    protected boolean moveIsLegal(int column)
    {
        // Loops through all rows in the board.
        for (int row = 0; row < this.board.getNumRows(); row++)
        {
            // If a piece is not found within a particular row
            // in the column, then a move is legal.
            if (!board.pieceExists(row, column))
            {
                return (true);
            }
        }
        
        // No empty cells were found, so the move is not legal.
        return (false);
    }
    
    /**
     * Determines if any winning positions exist on the Board.
     * 
     * @return True if there is a win; false otherwise.
     */
    protected boolean checkForWin()
    {
        // Loops through all Players to determine if any
        // have played a winning move.
        for (Player player: players)
        {
            if (checkForWin(player.getPiece()))
            {
                return (true);
            }
        }
        
        return (false);
    }
    
    /**
     * Determines if a Player has any winning positions on
     * the Board.
     * 
     * @param checkPiece The piece of the Player to use to check for
     * winning positions.
     * 
     * @return True if there is a win; false otherwise.
     */
    protected boolean checkForWin(char checkPiece)
    {
        // By default, horizontal lineups, vertical lineups, and
        // diagonal lineups of the appropriate length results in
        // a win.
        if (horizontalWin(checkPiece) || verticalWin(checkPiece) || 
            diagonalWin(checkPiece))
        {
            return (true);
        }
        else
        {
            return (false);
        }
    }
    
    /**
     * Determines if a game ends in a tie.
     * 
     * @return True if the players tied; false otherwise.
     */
    protected boolean checkForTie()
    {
        // Variable declaration.
        int winCount = 0;
        
        // Loops through all players.  If a player has
        // a winning position, the win count is incremented
        // by 1.
        for (Player player: getPlayers())
        {
            if (checkForWin(player.getPiece()))
            {
                winCount++;
            }
        }
        
        // If the board is full and there isn't a single winner
        // or if there are more than one winners, the game
        // ends in a tie.
        if ( (board.boardIsFull() && winCount != 1) || winCount > 1)
        {
            return (true);
        }
        else
        {
            return (false);
        }
    }
    
    /**
     * Determines if a Player has any winning horizontal positions on the Board.
     * 
     * @param checkPiece The piece of the Player to use to check for
     * winning positions.
     * 
     * @return True if there is a horizontal win; false otherwise.
     */
    protected boolean horizontalWin(char checkPiece)
    {
        // Number of times a row should be checked.
        int numLoops = board.getNumColumns() - winLength + 1;
        
        // Checks each row in the board for a winning position.
        for (int row = 0; row < board.getNumRows(); row++)
        {
            // Checks each possible winning position in the row.
            for (int check = 0; check < numLoops; check++)
            {
                // Pieces are identical by default.
                boolean identical = true;
                
                // Checks each piece in the potentially winning position.
                for (int piece = check; piece < check + winLength - 1 && 
                     identical ; piece++)
                {
                    // If two pieces do not match or if a piece is not the 
                    // current Player's piece, this particular position is not
                    // a win.
                    if (board.getPieceAt(row, piece) != board.getPieceAt(row, piece + 1)
                        || board.getPieceAt(row, piece) != checkPiece)
                    {
                        identical = false;
                    }
                }
                
                // If no differing pieces were found and if none of the spots
                // on the board in the position are empty, a winning position
                // has been found.
                if (identical)
                {
                    return (true);
                }
            }
        }
        
        // No winning positions were found, so no horizontal wins exist.
        return (false);
    }
    
    /**
     * Determines if a Player has any winning vertical positions on the Board.
     * 
     * @param checkPiece The piece of the Player to use to check for
     * winning positions.
     * 
     * @return True if there is a vertical win; false otherwise.
     */
    protected boolean verticalWin(char checkPiece)
    {
        // Number of times a column should be checked.
        int numLoops = board.getNumRows() - winLength + 1;
        
        // Checks each column in the board for a winning position.
        for (int column = 0; column < board.getNumColumns(); column++)
        {
            // Checks each possible winning position in the column.
            for (int check = 0; check < numLoops; check++)
            {
                // Pieces are identical by default.
                boolean identical = true;
                
                // Checks each piece in the potentially winning position.
                for (int piece = check; piece < check + winLength - 1 && 
                     identical ; piece++)
                {
                    // If two pieces do not match or if a piece is not the 
                    // current Player's piece, this particular position is not
                    // a win.
                    if (board.getPieceAt(piece, column) != board.getPieceAt(piece + 1, column)
                        || board.getPieceAt(piece, column) != checkPiece)
                    {
                        identical = false;
                    }
                }
                
                // If no differing pieces were found and if none of the spots
                // on the board in the position are empty, a winning position
                // has been found.
                if (identical)
                {
                    return (true);
                }
            }
        }
        
        // No winning positions were found, so no horizontal wins exist.
        return (false);
    }
    
    /**
     * Determines if a Player has any winning diagonal positions on the Board.
     * 
     * @param checkPiece The piece of the Player to use to check for
     * winning positions.
     * 
     * @return True if there is a diagonal win; false otherwise.
     */
    protected boolean diagonalWin(char checkPiece)
    {
        // Number of times rows/columns should be checked.
        // "Chunks" of the board are checked at one time 
        // (due to the multi-column and multi-row nature of diagonal lines).
        int numRowChecks = board.getNumRows() - winLength + 1;
        int numColChecks = board.getNumColumns() - winLength + 1;
        
        // Checks each "chunk" of rows for winning moves.
        for (int row = 0; row < numRowChecks; row++)
        {
            // Checks each "chunk" of columns in the row "chunk" for winning moves.
            for (int column = 0; column < numColChecks; column++)
            {
                // Pieces are identical by default.
                boolean identical = true;
                
                // Checks each piece in a potentially winning position.
                // This loop checks the diagonal line that runs from top-left to bottom-right
                // of the region currently being examined.
                for (int rowCheck = row, colCheck = column; rowCheck < row + winLength - 1 && 
                     colCheck < column + winLength - 1 && identical; rowCheck++, colCheck++)
                {
                    // If two pieces do not match or if a piece is not the 
                    // current Player's piece, this particular position is not
                    // a win.
                    if (board.getPieceAt(rowCheck, colCheck) != board.getPieceAt(rowCheck + 1, colCheck + 1) 
                        || board.getPieceAt(rowCheck, colCheck) != checkPiece)
                    {
                        identical = false;
                    }
                }
                
                // If no differing pieces were found and if none of the spots
                // on the board in the position are empty, a winning position
                // has been found.
                if (identical)
                {
                    return (true);
                }
                
                // Resets check variable.
                identical = true;
                
                // Checks each piece in a potentially winning position.
                // This loop checks the diagonal line that runs from bottom-left to top-right
                // of the region currently being examined.
                for (int rowCheck = row + winLength - 1, colCheck = column; rowCheck > row  && 
                     colCheck < column + winLength - 1 && identical; rowCheck--, colCheck++)
                {
                    // If two pieces do not match or if a piece is not the 
                    // current Player's piece, this particular position is not
                    // a win.
                    if (board.getPieceAt(rowCheck, colCheck) != board.getPieceAt(rowCheck - 1, colCheck + 1) 
                        || board.getPieceAt(rowCheck, colCheck) != checkPiece)
                    {
                        identical = false;
                    }
                }
                
                // If no differing pieces were found and if none of the spots
                // on the board in the position are empty, a winning position
                // has been found.
                if (identical)
                {
                    return (true);
                }
            }
        }
        
        // No winning positions were found, so no diagonal wins exist.
        return (false);
    }
}
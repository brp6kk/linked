package eyc.game.linkedgame.modes;

import eyc.game.linkedgame.Game;
import eyc.game.components.linkedgame.*;
import eyc.exception.*;
import eyc.input.*;
import java.util.*;

/**
 * Class that represents the Pop-Out variant of Linked.
 * 
 * Each turn, players have the alternative option of removing
 * one of their pieces from the bottom of the grid.  All pieces
 * above this discarded piece then shift down.
 */
public class PopOut extends Game
{
    // No unique instance variables.
    
    
    /********** Constructors **********/
    
    /**
     * Parameterized constructor for the PopOut class.
     * 
     * @param boardRows The number of rows for the Game's Board.
     * 
     * @param boardColumns The number of columns for the 
     * Game's Board.
     * 
     * @param players An array containing the Players of the Game.
     */
    public PopOut(int boardRows, int boardColumns, Player[] players)
    throws NonPositiveNumberException
    {
        super(boardRows, boardColumns, players);
    }
    
    
    /********** Accessors/Getters **********/
    
    // No unique getters necessary for this class.
    
    
    /********** Mutators/Setters **********/
    
    // No unique setters necessary for this class.
    
    
    /********** Other Methods **********/
    
    /**
     * Makes a single move.
     * Overrides the parent class's makeMove method, since
     * this variant has multiple possible move options.
     * 
     * @param piece The piece to be placed in this move.
     * 
     * @param input Scanner to read in user input.
     */
    @Override
    protected void makeMove(char piece, Scanner input)
    {
        // Variable declarations.
        int choice = 1; // Regular move by default.
        String prompt = ("Enter \"1\" to make a regular move.\n" +
                         "Enter \"2\" to remove a piece.\n" +
                         "Enter a number: ");
        
        // Prompt for regular move vs. alternative move is not
        // necessary when the alternative move is not possible.
        if (altMoveIsPossible(piece))
        {
            choice = NumIn.getInt(1, 2, prompt, input);
        }
        
        // If user entered 1 or if an alternative move is not
        // possible, they will make a normal move.
        // If user entered 2, they will make the alternative move.
        switch (choice)
        {
            case 1:
                super.makeMove(piece, input);
                break;
            case 2:
                makeAltMove(piece, input);
                break;
        }
    }
    
    /**
     * Makes the alternative move - "pops out" one of the user's
     * pieces from the bottom.
     * 
     * @param piece The piece to be removed in this move.
     * 
     * @param input Scanner to read in user input.
     */
    private void makeAltMove(char piece, Scanner input)
    {
        // Variable declarations.
        boolean moveMade = false;
        int removeColumn;
        String prompt = ("Enter the column to remove a piece from, or " + 
                         getBoard().getNumColumns() + " to go back: ");
        
        // Loops until a valid move is entered.
        do
        {
            // Gets column that user wants to remove piece from.
            removeColumn = NumIn.getInt(0, getBoard().getNumColumns(), 
                                        prompt, input);
            
            // If user wants to go back, the original makeMove method is
            // reaccessed and no alternative move is made.
            if (removeColumn == getBoard().getNumColumns())
            {
                this.makeMove(piece, input);
                moveMade = true;
            }
            // If move is legal, removes the piece and shifts down all
            // pieces above the removed piece.
            else if (altMoveIsLegal(piece, removeColumn))
            {
                // Loops through all rows except for row 0.
                // The piece in each row is replaced by the piece in the
                // previous row.
                for (int row = getBoard().getNumRows() - 1; row > 0; row--)
                {
                    // First, the piece in the current spot is removed.
                    // Then, the piece in the row directly above this spot
                    // is copied to the current spot.
                    getBoard().removePiece(row, removeColumn);
                    getBoard().placePiece(getBoard().getPieceAt(row - 1, removeColumn), 
                                          row, removeColumn);
                }
                
                // Row 0 will always be empty after a successful pop-out move.
                getBoard().placePiece(getBoard().emptyValue(), 0, removeColumn);
                moveMade = true;
            }
            // If move is not legal, player is alerted and loop
            // executes once more.
            else
            {
                System.out.println("You can only remove your own pieces " +
                                   "from the board.");
                moveMade = false;
            }
        } while (!moveMade);
    }
    
    /**
     * Determines if the alternative move is possible.
     * 
     * @param piece The piece to be used in this move.
     * 
     * @return True if an alternative move is possible;
     * false if such a move is not possible.
     */
    private boolean altMoveIsPossible(char piece)
    {
        // Loops through all columns in the board.
        for (int column = 0; column < getBoard().getNumColumns(); column++)
        {
            // If any potential alternative moves are legal,
            // an alternative move is possible.
            if (altMoveIsLegal(piece, column))
            {
                return (true);
            }
        }
        
        // No instances of the current piece was found; 
        // the alternative move is not possible this round.
        return (false);
    }
    
    /**
     * Determines if a given alternative move is allowed.
     * 
     * @param piece The current piece being played.
     * 
     * @param column The column to be checked.
     * 
     * @return True if the piece can be removed; false otherwise.
     */
    private boolean altMoveIsLegal(char piece, int column)
    {
        // The piece at the bottom of a column has to
        // be equal to the current player's piece in 
        // order for the alternative move to be legal.
        if (getBoard().getPieceAt(getBoard().getNumRows() - 1, column) == piece)
        {
            return (true);
        }
        else
        {
            return (false);
        }
    }
}
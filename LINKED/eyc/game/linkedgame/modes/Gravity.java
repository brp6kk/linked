package eyc.game.linkedgame.modes;

import eyc.game.linkedgame.Game;
import eyc.game.components.linkedgame.*;
import eyc.exception.*;
import eyc.input.*;
import java.util.*;

/**
 * Class that represents the Gravity variant of Linked.
 * 
 * After each turn, there's a chance that the "gravity" of the
 * board will switch.  In normal gravity, the pieces drop from the
 * top of the grid and fall to the bottom.  In inverted gravity, the
 * pieces drop from the bottom of the grid and rise to the top. 
 */
public class Gravity extends Game
{
    // Instance variables.
    private boolean normalGravity;
    
    
    /********** Constructors **********/
    
    /**
     * Parameterized constructor for the Gravity class.
     * 
     * @param boardRows The number of rows for the Game's Board.
     * 
     * @param boardColumns The number of columns for the 
     * Game's Board.
     * 
     * @param players An array containing the Players of the Game.
     */
    public Gravity (int boardRows, int boardColumns, Player[] players)
    throws NonPositiveNumberException
    {
        super(boardRows, boardColumns, players);
        
        this.normalGravity = true;
    }
    
    
    /********** Accessors/Getters **********/
    
    // No unique getters necessary for this class.
    
    
    /********** Mutators/Setters **********/
    
    // No unique setters necessary for this class.
    
    
    /********** Other Methods **********/
    
    /**
     * Makes a single move.
     * Overrides the parent class's makeMove method, since
     * this variant has multiple possible move options.
     * 
     * @param piece The piece to be placed in this move.
     * 
     * @param input Scanner to read in user input.
     */
    @Override
    protected void makeMove(char piece, Scanner input)
    {
        // If gravity is normal, a normal move is to be made.
        if (normalGravity)
        {
            super.makeMove(piece, input);
        }
        // If gravity is inverted, an inverted move is to be made.
        else
        {
            makeMoveInverted(piece, input);
        }
        
        // Generate new gravity.
        gravityGenerator();
    }
    
    /**
     * Makes an inverted move if gravity is inverted.
     * 
     * @param piece The piece to be placed in this move.
     * 
     * @param input Scanner to read in user input.
     */
    private void makeMoveInverted(char piece, Scanner input)
    {
        // Variable declarations.
        boolean moveMade = false;
        int dropColumn;
        String prompt = "Enter the column to drop a piece into: ";
        
        // Loops until a valid move is entered.
        do
        {
            // Gets column that user wants to drop piece into.
            dropColumn = NumIn.getInt(0, getBoard().getNumColumns() - 1, prompt, input);
            
            // If move is legal, drop the piece.
            if (moveIsLegal(dropColumn))
            {
                // Variable declarations.
                int dropRow = getBoard().getNumRows() - 1;
                boolean canDrop = false;
                
                // Loops until the highest row is found.
                while (!canDrop)
                {
                    // No piece in this row; check next row.
                    // Stops checking rows if the top of the board
                    // is reached.
                    if(!getBoard().pieceExists(dropRow, dropColumn) &&
                       !(dropRow < 0))
                    {
                        dropRow--;
                    }
                    // A piece is in this row; the new piece will
                    // be placed in the row below this row.
                    else
                    {
                        dropRow++;
                        canDrop = true;
                    }
                }
                
                // Adds piece to proper row.
                getBoard().placePiece(piece, dropRow, dropColumn);
                moveMade = true;
            }
            // If move is not legal, player is alerted and loop
            // executes once more.
            else
                {
                    System.out.println("Column is full, select another.");
                    moveMade = false;
                }
        } while (!moveMade);
    }
    
    /**
     * Updates the gravity of the game.  Either gravity
     * will remain the same, or will become inverted.
     */
    private void gravityGenerator()
    {
        // A random number between 1 and 100 is generated.
        int randNum = (int)(Math.random() * 100) + 1;
        
        // On Earth, gravity = 32 feet per second per second.
        // This is an arbitrary value that determines when gravity
        // is inverted.
        if (randNum <= 32)
        {
            normalGravity = !normalGravity;
            flipBoard();
        }
        
        // System.out.println(randNum + ": " + normalGravity);
        // ^ Used for testing purposes.
    }
    
    /**
     * Inverts the board to correspond to the current gravity.
     */
    private void flipBoard()
    {   
        // Variable declarations.
        int rowIndex, move, endIndex;
        
        // If gravity is inverted, start flip at the top of the board
        // and work downwards.
        if (!normalGravity)
        {
            rowIndex = 0;
            move = 1;
            endIndex = getBoard().getNumRows();
        }
        // If gravity is normal, start flip at the bottom of the board
        // and work upwards.
        else
        {
            rowIndex = getBoard().getNumRows() - 1;
            move = -1;
            endIndex = -1;
        }
        
        // Loops through all columns in the board to flip them all.
        for (int column = 0; column < getBoard().getNumColumns(); column++)
        {
            // Loops through all rows in the column.
            for (int row = rowIndex; row != endIndex; row += move)
            {
                // If there is a piece in the row, move it appropriately.
                if (getBoard().pieceExists(row, column))
                {
                    // Loops until the first row is reached.
                    for (int moveIndex = row; moveIndex != rowIndex; moveIndex -= move)
                    {
                        // If a piece does not exist in the previous row,
                        // place the current piece there and remove it from
                        // this row.
                        if (!getBoard().pieceExists(moveIndex - move, column))
                        {
                            getBoard().placePiece(getBoard().getPieceAt(moveIndex, column), 
                                                  moveIndex - move, column);
                            getBoard().removePiece(moveIndex, column);
                        }
                    }
                }
            }
        }
    }
}
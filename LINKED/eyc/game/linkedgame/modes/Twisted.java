package eyc.game.linkedgame.modes;

import eyc.game.linkedgame.Game;
import eyc.game.components.linkedgame.*;
import eyc.exception.*;
import eyc.input.*;
import java.util.*;

/**
 * Class that represents the Twisted variant of Linked.
 * 
 * Each turn, players have the alternative option of "twisting"
 * a row by shifting each piece either one column to the
 * left or one column to the right.
 */
public class Twisted extends Game
{
    // No unique instance variables.
    
    
    /********** Constructors **********/
    
    /**
     * Parameterized constructor for the Twisted class.
     * 
     * @param boardRows The number of rows for the Game's Board.
     * 
     * @param boardColumns The number of columns for the 
     * Game's Board.
     * 
     * @param players An array containing the Players of the Game.
     */
    public Twisted(int boardRows, int boardColumns, Player[] players)
    throws NonPositiveNumberException
    {
        super(boardRows, boardColumns, players);
    }
    
    
    /********** Accessors/Getters **********/
    
    // No unique getters necessary for this class.
    
    
    /********** Mutators/Setters **********/
    
    // No unique setters necessary for this class.
    
    
    /********** Other Methods **********/
    
    /**
     * Makes a single move.
     * Overrides the parent class's makeMove method, since
     * this variant has multiple possible move options.
     * 
     * @param piece The piece to be placed in this move.
     * 
     * @param input Scanner to read in user input.
     */
    @Override
    protected void makeMove(char piece, Scanner input)
    {
        // Variable declarations.
        int choice = 1; // Regular move by default.
        String prompt = ("Enter \"1\" to make a regular move.\n" +
                         "Enter \"2\" to shift a row.\n" +
                         "Enter a number: ");
        
        // Promopt for regular move vs. alternative move is not
        // necessary when the alternative move is not possible.
        if (altMoveIsPossible())
        {
            choice = NumIn.getInt(1, 2, prompt, input);
        }
        
        // If user entered 1 or if an alternative move is not
        // possible, they will make a normal move.
        // If user entered 2, they will make the alternative move.
        switch (choice)
        {
            case 1:
                super.makeMove(piece, input);
                break;
            case 2:
                makeAltMove(piece, input);
                break;
        }
    }
    
    /**
     * Draws board.
     * Overrides the parent class's drawBoard method,
     * since row numbers need to be drawn in this variant.
     */
    @Override
    public void drawBoard()
    {
        getBoard().drawBoard(true);
    }
    
    /**
     * Makes the alternative move - "twists" one of the rows.
     * 
     * @param input Scanner to read in user input.
     */
    private void makeAltMove(char piece, Scanner input)
    {
        // Variable declarations.
        boolean validInput = false;
        int shiftRow = 0, directionNum = 0, direction = 0;
        String prompt = ("Enter the row to twist, or " + 
                         getBoard().getNumRows() + " to go back: ");
        
        // Loops until a valid move is entered.
        do
        {
            // Gets row that user wants to shift.
            shiftRow = NumIn.getInt(0, getBoard().getNumRows(),
                                    prompt, input);
            
            // If user wants to go back, the original makeMove is
            // reaccessed and no alternative move is made.
            if (shiftRow == getBoard().getNumRows())
            {
                this.makeMove(piece, input);
                return;
            }
            // If move is legal, loop is exitted.
            else if (altMoveIsLegal(shiftRow))
            {
                validInput = true;
            }
            // If move is not legal, player is alerted and loop
            // executes once more.
            else
            {
                System.out.println("Only full rows can be twisted.");
                validInput = false;
            }
        } while (!validInput);
        
        // Resets variables appropriately.
        validInput = false;
        prompt = ("Enter \"1\" to shift this row to the left.\n" +
                  "Enter \"2\" to shift this row to the right.\n" +
                  "Enter \"3\" to start this move over.\n" +
                  "Enter a number: ");
        
        directionNum = NumIn.getInt(1, 3, prompt, input);
        
        // Sets variables based on user input and shifts the row.
        if (directionNum == 1)
        {
            direction = 1;
        }
        else if (directionNum == 2)
        {
            direction = -1;
        }
        else if (directionNum == 3) // User wants to go back.
        {
            this.makeMove(piece, input);
            return;
        }
        
        shift(shiftRow, direction);
    }
    
    /**
     * Determines if the alternative move is possible.
     * 
     * @return True if an alternative move is possible;
     * false if such a move is not possible.
     */
    private boolean altMoveIsPossible()
    {
        // Loops through all rows in the board.
        for (int row = 0; row < getBoard().getNumRows(); row++)
        {
            // If any of the rows can be shifted, the alternative
            // move is possible.
            if (altMoveIsLegal(row))
            {
                return (true);
            }
        }
        
        // No rows are able to be shifted; the alternative move
        // is not possible this found.
        return (false);
    }
    
    /**
     * Determines if a given alternative move is allowed.
     * 
     * @param row The row to be checked.
     * 
     * @return True if the row can be shifted; false otherwise.
     */
    private boolean altMoveIsLegal(int row)
    {
        // Row can be shifted if the entire row is completely full.
        for (int column = 0; column < getBoard().getNumColumns(); column++)
        {
            if (!getBoard().pieceExists(row, column))
            {
                return (false);
            }
        }
        
        // The entire row is full.
        return (true);
    }
    
    /**
     * Moves the pieces in one row.
     * 
     * @param row The row to shift.
     * 
     * @param direction How to shift the row; a positive integer is
     * a left shift, and a negative integer is a right shift; the
     * absolute value of direction is how big of a shift will occur.
     */
    private void shift(int row, int direction)
    {
        // Variable declarations.
        int colIndex, endIndex;
        
        // For a right-shift, start on the right-hand side
        // and end on the left.
        if (direction < 0)
        {
            colIndex = getBoard().getNumColumns() - 1;
            endIndex = 0;
        }
        // For a left-shift, start on the left-hand side
        // and end on the right.
        else
        {
            colIndex = 0;
            endIndex = getBoard().getNumColumns() - 1;
        }
        
        // Temporarily store the starting piece.
        char temp = getBoard().getPieceAt(row, colIndex);
        
        // Remove the piece at the current position and replace it
        // with the piece beside it (depending on direction).
        for ( ; colIndex != endIndex; colIndex += direction)
        {
            getBoard().removePiece(row, colIndex);
            getBoard().placePiece(getBoard().getPieceAt(row, colIndex + direction),
            row, colIndex);
        }
        
        // Starting piece is placed at the opposite end of the row.
        getBoard().removePiece(row, colIndex);
        getBoard().placePiece(temp, row, colIndex);
    }
}
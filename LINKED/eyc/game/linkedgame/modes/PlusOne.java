package eyc.game.linkedgame.modes;

import eyc.game.linkedgame.Game;
import eyc.game.components.linkedgame.*;
import eyc.exception.*;

/**
 * Class that represents the Plus One variant of Linked.
 * 
 * Same as Classic, but a player wins once they line up five of their
 * own pieces rather than four.
 */
public class PlusOne extends Game
{
    // No unique instance variables.
    
    
    /********** Constructors **********/
    
    /**
     * Parameterized constructor for the PlusOne class.
     * 
     * @param boardRows The number of rows for the Game's Board.
     * 
     * @param boardColumns The number of columns for the
     * Game's Board.
     * 
     * @param players An array containing the Players of the Game.
     */
    public PlusOne (int boardRows, int boardColumns, Player[] players)
    throws NonPositiveNumberException
    {
        // In this variant, 5 pieces in a row are necessary
        // for a win.
        super(boardRows, boardColumns, players, 5);
    }
    
    
    /********** Accessors/Getters **********/
    
    // No unique getters necessary for this class.
    
    
    /********** Mutators/Setters **********/
    
    // No unique setters necessary for this class.
    
    
    /********** Other Methods **********/
    
    // No other unique methods necessary for this class.
}
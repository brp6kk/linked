package eyc.game.linkedgame.modes;

import eyc.game.linkedgame.Game;
import eyc.game.components.linkedgame.*;
import eyc.exception.*;

/**
 * Class that represents the ZigZag variant of Linked.
 * 
 * Same as Classic, but a player wins once they line up their
 * pieces diagonally.  Horizontal and vertical wins are not 
 * possible.
 */
public class ZigZag extends Game
{
    // No unique instance variables.
    
    
    /********** Constructors **********/
    
    /**
     * Parameterized constructor for the ZigZag class.
     * 
     * @param boardRows The number of rows for the Game's Board.
     * 
     * @param boardColumns The number of columns for the
     * Game's Board.
     * 
     * @param players An array containing the Players of the Game.
     */
    public ZigZag(int boardRows, int boardColumns, Player[] players)
    throws NonPositiveNumberException
    {
        super(boardRows, boardColumns, players);
    }
    
    
    /********** Accessors/Getters **********/
    
    // No unique getters necessary for this class.
    
    
    /********** Mutators/Setters **********/
    
    // No unique setters necessary for this class.
    
    
    /********** Other Methods **********/
    
    /**
     * Determines if a Player has any winning positions on the Board.
     * Overrides the parent class's checkForMove method, since
     * only diagonal wins are valid.
     * 
     * @param checkPiece The piece of the Player to use to check for
     * winning positions.
     * 
     * @return True if there is a win; false otherwise.
     */
    @Override
    protected boolean checkForWin(char checkPiece)
    {
        // Parent class's diagonalWin() is the only check necessary.
        if (diagonalWin(checkPiece))
        {
            return (true);
        }
        else
        {
            return (false);
        }
    }
}
package eyc.game.linkedgame.modes;

import eyc.game.linkedgame.Game;
import eyc.game.components.linkedgame.*;
import eyc.exception.*;

/**
 * Class that represents the Classic variant of Linked.
 * 
 * Players "drop" pieces into a column in a grid, which then fall
 * to the lowest unoccupied space.  A player wins once they line
 * up four of their own pieces horizontally, vertically, or
 * diagonally.
 */
public class Classic extends Game
{
    // No unique instance variables.
    
    
    /********** Constructors **********/
    
    /**
     * Parameterized constructor for the Classic class.
     * 
     * @param boardRows The number of rows for the Game's Board.
     * 
     * @param boardColumns The number of columns for the
     * Game's Board.
     * 
     * @param players An array containing the Players of the Game.
     */
    public Classic (int boardRows, int boardColumns, Player[] players)
    throws NonPositiveNumberException
    {
        super(boardRows, boardColumns, players);
    }
    
    /********** Accessors/Getters **********/
    
    // No unique getters necessary for this class.
    
    
    /********** Mutators/Setters **********/
    
    // No unique setters necessary for this class.
    
    /********** Other Methods **********/
    
    // No other unique methods necessary for this class.
}